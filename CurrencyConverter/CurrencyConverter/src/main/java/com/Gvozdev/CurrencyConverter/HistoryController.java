package com.Gvozdev.CurrencyConverter;

import com.Gvozdev.CurrencyConverter.domain.History;
import com.Gvozdev.CurrencyConverter.repos.HistoryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class HistoryController {
    @Autowired
    private HistoryRepo historyRepo;

    @GetMapping("/history")
    public String loadHistory(Model model) {
        Iterable<History> historyIterable = historyRepo.findAll();
        List<List<String>> allEntries = new ArrayList<>();
        int counter = 0;
        for (var history : historyIterable) {
            List<String> entry = new ArrayList<>();
            String sourceValute = history.getSourceValute();
            String targetValute = history.getTargetValute();
            Double sourceSum = history.getSourceSum();
            Double convertedSum = history.getConvertedSum();
            String queryDate = history.getQueryDate();
            entry.add(sourceValute);
            entry.add(targetValute);
            entry.add(String.valueOf(sourceSum));
            entry.add(String.valueOf(convertedSum));
            entry.add(queryDate);
            model.addAttribute("entry" + counter, entry);
            counter++;
            allEntries.add(entry);
        }

        model.addAttribute("allEntries", allEntries);

        return "history";
    }
}

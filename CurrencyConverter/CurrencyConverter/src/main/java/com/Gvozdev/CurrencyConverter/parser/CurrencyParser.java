package com.Gvozdev.CurrencyConverter.parser;

import com.Gvozdev.CurrencyConverter.domain.Currency;
import com.Gvozdev.CurrencyConverter.repos.CurrencyRepo;
import org.jsoup.Jsoup;
import org.jsoup.helper.W3CDom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.util.*;

@Service
public class CurrencyParser {

    @Autowired
    private CurrencyRepo currencyRepo;

    public void handle() throws IOException {
        org.jsoup.nodes.Document document = Jsoup.connect("http://www.cbr.ru/scripts/XML_daily.asp").get();
        W3CDom w3CDom = new W3CDom();
        org.w3c.dom.Document w3cDoc = w3CDom.fromJsoup(document);
        NodeList nodeListID = w3cDoc.getElementsByTagName("Valute");
        NodeList nodeListNumCode = w3cDoc.getElementsByTagName("NumCode");
        NodeList nodeListCharCode = w3cDoc.getElementsByTagName("CharCode");
        NodeList nodeListName = w3cDoc.getElementsByTagName("Name");
        NodeList nodeListValue = w3cDoc.getElementsByTagName("Value");

        setCurrencyRepo(nodeListID, nodeListNumCode, nodeListCharCode, nodeListName, nodeListValue);
    }

    public void setCurrencyRepo(
            NodeList nodeListID,
            NodeList nodeListNumCode,
            NodeList nodeListCharCode,
            NodeList nodeListName,
            NodeList nodeListValue
    ) {
        int currencyCounter = 0;
        int currencyAmount = 34;
        while (currencyCounter < currencyAmount) {
            Element elementId = (Element) nodeListID.item(currencyCounter);
            String currencyId = elementId.getAttribute("ID");

            Element elementNumCode = (Element) nodeListNumCode.item(currencyCounter);
            Integer currencyNumCode = Integer.parseInt(elementNumCode.getTextContent());

            Element elementCharCode = (Element) nodeListCharCode.item(currencyCounter);
            String currencyCharCode = elementCharCode.getTextContent();

            Element elementName = (Element) nodeListName.item(currencyCounter);
            String currencyName = elementName.getTextContent();

            Element elementValue = (Element) nodeListValue.item(currencyCounter);
            String currencyValue = elementValue.getTextContent();

            Currency currency = new Currency();
            currency.setCurrencyId(currencyId);
            currency.setNumCode(currencyNumCode);
            currency.setCharCode(currencyCharCode);
            currency.setName(currencyName);
            currency.setValue(currencyValue);

            currencyRepo.save(currency);
            currencyCounter++;
        }
    }

    public static List<Node> asList(NodeList nodeList) {
        if (nodeList.getLength() == 0)
            return Collections.<Node>emptyList();
        else
            return new NodeListWrapper(nodeList);
    }

    static final class NodeListWrapper extends AbstractList<Node> implements RandomAccess {
        private final NodeList nodeList;

        NodeListWrapper(NodeList nodeList) {
            this.nodeList = nodeList;
        }

        public Node get(int index) {
            return nodeList.item(index);
        }

        public int size() {
            return nodeList.getLength();
        }
    }
}

package com.Gvozdev.CurrencyConverter;

import com.Gvozdev.CurrencyConverter.parser.CurrencyParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class DBController {

    @Autowired
    private CurrencyParser currencyParser;

    @RequestMapping
    public void setDataInDB() throws IOException {
        currencyParser.handle();
    }
}
